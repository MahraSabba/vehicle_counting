import os
import json
import traceback


class Config:

    def __init__(self):
        self.config_name = "base_config.json"
        self.cwd_path = os.getcwd()
        self.config_folder = os.path.join(self.cwd_path, "data", "config")
        self.config_path = os.path.join(self.config_folder, self.config_name)
        self.config = json.load(open(self.config_path, 'r'))

        self._dont_remove_this_config = ".config"
        self._default_config = os.path.join(self.cwd_path, "Storage", "config", self._dont_remove_this_config)

    """API calls start here"""

    def get_api_loc(self):
        return self.config['api_calls']['script_loc']

    def get_api_lang(self):
        return self.config['api_calls']['lang']

    def get_host_server(self):
        return self.config['api_calls']['host_server']

    def get_send_json(self):
        return self.config['api_calls']['send_json']

    def get_send_status(self):
        return self.config['api_calls']['send_status']

    """END"""

    """Folder calls start here"""

    def get_combined_json_folder(self):
        return "{}/{}".format(self.config['folders']['storage'],
                              self.config['folders']['combined_json'])

    def get_captured_images_folder(self):
        return self.config['folders']['captured_images']

    def get_untracked_images_folder(self):
        return self.config['folders']['untracked_images']

    def get_toll_counting_folder(self):
        return self.config['folders']['toll_counting']

    def get_counting_folder(self):
        return self.config['folders']['counting']

    def get_classified_folder(self):
        return self.config['folders']['classified']

    def get_json_folder(self):
        return "{}/{}".format(self.config['folders']['storage'],
                              self.config['folders']['jsons'])

    def get_storage_folder(self):
        return self.config['folders']['storage']

    def get_tf_classifier_folder(self):
        return os.path.join(self.config['folders']['models'],
                            self.config['folders']['classifiers'],
                            self.config['folders']['tf'])

    def get_status_folder(self):
        return os.path.join(self.config['folders']['storage'],
                            self.config['folders']['status'])

    def get_resources_folder(self):
        return self.config['folders']['resources']

    def get_read_files_folder(self):
        return os.path.join(self.get_storage_folder(),
                            self.config['folders']['readfiles'])

    def get_read_images_folder(self):
        return os.path.join(self.get_storage_folder(),
                            self.config['folders']['readimages'])

    def get_trackers_folder(self):
        return os.path.join(self.config['folders']['storage'],
                            self.config['folders']['trackers'])

    def get_initial_condition_location(self):
        return os.path.join(self.config['folders']['storage'],
                            self.config['folders']['initial_conditions'])

    """END"""

    """File calls start here"""

    def get_label_id_file(self):
        return os.path.join(self.config['folders']['traffic'],
                            self.config['files']['label_id'])

    def get_process_per_gpu(self):
        return os.path.join(self.config['folders']['labels'],
                            self.config['files']['process_per_gpu'])

    def get_status(self):
        return os.path.join(self.get_status_folder(),
                            self.config['files']['status'])

    def get_classification_csv(self):
        return self.config['files']['classification']

    def get_time_file(self):
        return self.config['files']['time']

    def get_process_id(self):
        return os.path.join(self.get_status_folder(),
                            self.config['files']['process_id'])

    def get_data_json(self):
        return os.path.join(self.get_resources_folder(),
                            self.config['files']['data'])

    def get_read_file_log(self):
        return self.config['files']['readfilelog']

    def get_tracker_file_name(self):
        return self.config['files']['tracker_file_name']

    def get_tracker_data_file(self):
        return self.config['files']['tracker_data']

    def get_current_video_images_file(self):
        return self.config['files']['current_images']

    def get_labels_file(self):
        return os.path.join(self.config['folders']['storage'],
                            self.config['folders']['config'],
                            self.config['files']['labels'])

    """END"""

    """Tracker Calls Start here"""

    def get_similarity_model_location(self):
        return os.path.join(self.config['folders']['models'],
                            self.config['folders']['tracking'],
                            self.config['trackers']['similarity']['name'])

    def get_tsim_max_cosine_distance(self):
        return self.config['trackers']['similarity']['max_cosine_distance']

    def get_tsim_metric_type(self):
        return self.config['trackers']['similarity']['metric_type']

    def get_tsim_nms_overlap(self):
        return self.config['trackers']['similarity']['nms_max_overlap']

    def get_tsim_max_age(self):
        return self.config['trackers']['similarity']['max_age']

    def get_tsim_max_iou(self):
        return self.config['trackers']['similarity']['max_iou_distance']

    def get_tsim_n_init(self):
        return self.config['trackers']['similarity']['n_init']

    def get_tsim_encoder_batch_size(self):
        return self.config['trackers']['similarity']['encoder']['batch_size']

    def get_tsim_nn_budget(self):
        return self.config['trackers']['similarity']['nn_budget']

    def get_tsim_max_dead_tracker_limit(self):
        return self.config['trackers']['similarity']['tracker']['max_dead_tracker']

    def get_tsim_max_crops_allowed(self):
        return self.config['trackers']['similarity']['tracker']['max_crops_allowed']

    def get_tsim_null_image_name(self):
        return self.config['trackers']['similarity']['tracker']['null_image_name']

    def get_kalman_max_age(self):
        return self.config['trackers']['kalman']['max_age']

    def get_kalman_min_hits(self):
        return self.config['trackers']['kalman']['min_hits']

    def get_kalman_null_image_name(self):
        return self.config['trackers']['kalman']['null_image_name']

    def get_kalman_hits_limit(self):
        return self.config['trackers']['kalman']['hits_limit']

    def get_kalman_capture_limit(self):
        return self.config['trackers']['kalman']['capture_limit']

    def get_kalman_highest_speed(self):
        return self.config['trackers']['kalman']['highest_speed']

    def get_kalman_dim_x(self):
        return self.config['trackers']['kalman']['kalmanfilter']['dim_x']

    def get_kalman_dim_z(self):
        return self.config['trackers']['kalman']['kalmanfilter']['dim_z']

    def get_kalman_kf_r(self):
        return self.config['trackers']['kalman']['kalmanfilter']['kf_R']

    def get_kalman_kf_p(self):
        return self.config['trackers']['kalman']['kalmanfilter']['kf_P']

    def get_kalman_kf_px(self):
        return self.config['trackers']['kalman']['kalmanfilter']['kf_Px']

    def get_kalman_kf_q(self):
        return self.config['trackers']['kalman']['kalmanfilter']['kf_Q']

    def get_kalman_kf_xx(self):
        return self.config['trackers']['kalman']['kalmanfilter']['kf_xx']

    def get_kalman_iou_threshold(self):
        return self.config['trackers']['kalman']['iou_threshold']

    def get_kalman_delta_x(self):
        return self.config['trackers']['kalman']['delta_x']

    def get_kalman_threshold_score(self):
        return self.config['trackers']['kalman']['score_accuracy_threshold']

    def get_kalman_y_max_threshold_upper(self):
        return self.config['trackers']['kalman']['bbox_ymax_upper']

    def get_kalman_y_max_threshold_lower(self):
        return self.config['trackers']['kalman']['bbox_ymax_lower']

    """END"""

    "Detector calls start here"

    def get_detector(self):
        return self.config['detectors']

    def get_det_show_result(self):
        return self.get_detector()['show_result']

    def get_det_frame_name(self):
        return self.get_detector()['frame_name']

    def get_det_total_accountable_percentage(self):
        return self.get_detector()['detection_total_percentage']

    def get_det_logging_step(self):
        return self.get_detector()['detection_logging_step']

    def get_det_aoi_limit(self):
        return self.get_detector()['aoi_limit']

    def get_det_step_size(self):
        return self.get_detector()['step_size']

    def get_det_vis_properties(self):
        return self.get_detector()['visualization']

    def get_det_vis_box_color(self):
        return self.get_det_vis_properties()['box_color']

    def get_det_vis_box_thickness(self):
        return self.get_det_vis_properties()['box_thickness']

    def get_det_tf(self):
        return self.get_detector()['tf']

    def get_det_tf_max_processable_box(self):
        return self.get_det_tf()['max_processable_box']

    def get_det_tf_frame_y_max_thresh(self):
        return self.get_det_tf()['frame_y_max_thresh']

    def get_det_tf_frame_y_min_thresh(self):
        return self.get_det_tf()['frame_y_min_thresh']

    def get_det_tf_vehicle_properties(self):
        return self.get_det_tf()['vehicle']

    def get_det_tf_veh_min_bx(self):
        return self.get_det_tf_vehicle_properties()['min_bx']

    def get_det_tf_veh_min_by(self):
        return self.get_det_tf_vehicle_properties()['min_by']

    def get_det_tf_veh_max_bx(self):
        return self.get_det_tf_vehicle_properties()['max_bx']

    def get_det_tf_veh_max_by(self):
        return self.get_det_tf_vehicle_properties()['max_by']

    def get_det_tf_veh_thresh_score(self):
        return self.get_det_tf_vehicle_properties()['threshold_score']

    def get_det_tf_other_properties(self):
        return self.get_det_tf()['other']

    def get_det_tf_other_min_bx(self):
        return self.get_det_tf_other_properties()['min_bx']

    def get_det_tf_other_min_by(self):
        return self.get_det_tf_other_properties()['min_by']

    def get_det_tf_other_max_bx(self):
        return self.get_det_tf_other_properties()['max_bx']

    def get_det_tf_other_max_by(self):
        return self.get_det_tf_other_properties()['max_by']

    def get_det_tf_other_thresh_score(self):
        return self.get_det_tf_other_properties()['threshold_score']

    def get_det_tf_model_classes(self):
        return self.get_det_tf()['model_classes']

    def get_det_tf_ava_classes(self):
        return self.get_det_tf_model_classes()['ava']

    def get_det_tf_face_classes(self):
        return self.get_det_tf_model_classes()['face']

    def get_det_tf_oid_classes(self):
        return self.get_det_tf_model_classes()['oid']

    def get_det_tf_coco_classes(self):
        return self.get_det_tf_model_classes()['coco']

    def get_det_tf_kitti_classes(self):
        return self.get_det_tf_model_classes()['kitti']

    def get_det_tf_vehicles_classes(self):
        return self.get_det_tf_model_classes()['vehicles']

    def get_det_yolo(self):
        return self.get_detector()['yolo']

    def get_det_yolo_min_bx(self):
        return self.get_det_yolo()['min_bx']

    def get_det_yolo_min_by(self):
        return self.get_det_yolo()['min_by']

    def get_det_yolo_max_bx(self):
        return self.get_det_yolo()['max_bx']

    def get_det_yolo_max_by(self):
        return self.get_det_yolo()['max_by']

    def get_det_yolo_thresh_score(self):
        return self.get_det_yolo()['threshold_score']

    def get_det_yolo_max_processable_boxes(self):
        return self.get_det_yolo()['max_processable_box']

    def get_det_yolo_frame_y_max_thresh(self):
        return self.get_det_yolo()['frame_y_max_thresh']

    def get_det_yolo_frame_y_min_thresh(self):
        return self.get_det_yolo()['frame_y_min_thresh']

    def get_det_yolo_properties(self):
        return self.get_det_yolo()['properties']

    def get_det_yolo_model_path(self):
        return self.get_det_yolo_properties()['model_path']

    def get_det_yolo_anchor_path(self):
        return self.get_det_yolo_properties()['anchor_path']

    def get_det_yolo_classes_path(self):
        return self.get_det_yolo_properties()['classes_path']

    def get_det_yolo_score(self):
        return self.get_det_yolo_properties()['score']

    def get_det_yolo_iou(self):
        return self.get_det_yolo_properties()['iou']

    def get_det_yolo_image_size(self):
        return (self.get_det_yolo_properties()['model_image_size'],
                self.get_det_yolo_properties()['model_image_size'])

    def get_det_yolo_gpu_num(self):
        return self.get_det_yolo_properties()['gpu_num']

    def get_det_mrcnn(self):
        return self.get_detector()['mrcnn']

    def get_det_mrcnn_min_bx(self):
        return self.get_det_mrcnn()['min_bx']

    def get_det_mrcnn_min_by(self):
        return self.get_det_mrcnn()['min_by']

    def get_det_mrcnn_max_bx(self):
        return self.get_det_mrcnn()['max_bx']

    def get_det_mrcnn_max_by(self):
        return self.get_det_mrcnn()['max_by']

    def get_det_mrcnn_thresh(self):
        return self.get_det_mrcnn()['threshold_score']

    def get_det_mrcnn_max_processable_boxes(self):
        return self.get_det_mrcnn()['max_processable_box']

    def get_det_mrcnn_frame_y_max_thresh(self):
        return self.get_det_mrcnn()['frame_y_max_thresh']

    def get_det_mrcnn_frame_y_min_thresh(self):
        return self.get_det_mrcnn()['frame_y_min_thresh']

    def get_det_face(self):
        return self.get_detector()['face']

    def get_det_face_min_bx(self):
        return self.get_det_face()['min_bx']

    def get_det_face_min_by(self):
        return self.get_det_face()['min_by']

    def get_det_face_max_bx(self):
        return self.get_det_face()['max_bx']

    def get_det_face_max_by(self):
        return self.get_det_face()['max_by']

    def get_det_face_thresh(self):
        return self.get_det_face()['threshold_score']

    def get_det_face_max_processable_boxes(self):
        return self.get_det_face()['max_processable_box']

    def get_det_face_frame_y_max_thresh(self):
        return self.get_det_face()['frame_y_max_thresh']

    def get_det_face_frame_y_min_thresh(self):
        return self.get_det_face()['frame_y_min_thresh']

    def get_det_face_properties(self):
        return self.get_det_face()['properties']

    def get_det_face_prob_thresh(self):
        return self.get_det_face_properties()['prob_thresh']

    def get_det_face_nms_thresh(self):
        return self.get_det_face_properties()['nms_thresh']

    def get_det_face_lw(self):
        return self.get_det_face_properties()['lw']

    def get_det_face_max_input_dim(self):
        return self.get_det_face_properties()['max_input_dim']

    def get_det_face_name(self):
        return self.get_det_face_properties()['name']

    """END"""

    """Classifiers start here"""

    def get_classifiers(self):
        return self.config['classifiers']

    def get_top_x_results(self):
        return self.get_classifiers()['top_x_results']

    def get_clf_h(self):
        return self.get_classifiers()['h']

    def get_clf_w(self):
        return self.get_classifiers()['w']

    def get_clf_mean(self):
        return self.get_classifiers()['mean']

    def get_clf_std(self):
        return self.get_classifiers()['std']

    def get_clf_node_start(self):
        return self.get_classifiers()['node_start']

    def get_clf_imnet_tensor_shape(self):
        return self.get_classifiers()['imnet_tensor_shape']

    def get_densenet(self):
        return self.get_classifiers()['densenet']

    def get_densenet_class_path(self):
        return os.path.join(self.get_densenet()['resources'],
                            self.get_densenet()['class_file'])

    def get_densenet_m_red(self):
        return self.get_densenet()['model_red']

    def get_densenet_m_class(self):
        return self.get_densenet()['model_class']

    def get_densenet_m_loss(self):
        return self.get_densenet()['model_loss']

    def get_densenet_m_metric(self):
        return self.get_densenet()['model_metric']

    def get_densenet_s_lr(self):
        return self.get_densenet()['sgd_lr']

    def get_densenet_s_decay(self):
        return self.get_densenet()['sgd_decay']

    def get_densenet_s_momemtum(self):
        return self.get_densenet()['sgd_momentum']

    def get_densenet_s_nesterov(self):
        return self.get_densenet()['sgd_nesterov']

    def get_densenet_image_dim(self):
        return (self.get_densenet()['image_dim'],
                self.get_densenet()['image_dim'])
    """END"""

    """Base Getter function for additional functionality throughout the project."""
    def get(self, keys, default_value):

        """
        :param keys: Can be either a string or an array of strings.
                     If string, the keys can be separated via ';' symbol.
                     If an array, individual elements use the syntax of
                     the string mentioned above. The values returned from
                     this function will be a concatenation of all the values
                     requested via os.path.join().
        :param default_value: The default value if the keys are absent from
                     the config file. If 'None' is entered and the key is
                     absent, will check the .config file for the keys. If
                     absent from there, return 'None' and error.
        :return:
        """
        try:
            if keys is None:
                if default_value is None:
                    return default_value, "WAR:: No key given and no default value given."
                return default_value, "ERR:: No key given for {}".format(default_value)

            if isinstance(keys, str):
                return self._parse_string_key(keys, default_value)

            if isinstance(keys, list):
                return self._parse_array_key(keys, default_value)

        except Exception as exc:
            return None, "ERR:: key or default value are doing weird things"

    def _parse_array_key(self, keys, default_value):
        try:
            returnable_values = []
            default_values_for_input = []
            error_list = []
            if default_value is None:
                for i in range(len(keys)):
                    default_values_for_input[i] = None
            else:
                default_values_for_input = default_value.split('/')
            for i in range(len(keys)):
                ret_val, ret_err = self._parse_string_key(keys[i], default_values_for_input[i])
                returnable_values.append(ret_val)
                error_list.append(ret_err)
                if ret_err is not None:
                    if "ERR" in ret_err:
                        error = "ERR:: One of the keys inputted had an error.\n" \
                                "'{}' does not match '{}'.\n" \
                                "There might be more error in the data. Verify the " \
                                "inputs thoroughly.".format(keys[i],
                                                            default_values_for_input[i])
                        return default_value, error
            if all(err is None for err in error_list):
                error = None
                returnable_value = os.path.join(*returnable_values)
                return returnable_value, error
            elif any("WAR" in err for err in error_list if err is not None):
                error = "WAR:: The returned value is a mixture of base config and your config. Fix this"
                returnable_value = os.path.join(*returnable_values)
                return returnable_value, error
        except Exception as exc:
            traceback.print_exc()
            return None, "ERR:: No value returned for {} list of keys.".format(default_value)

    def _parse_string_key(self, keys, default_value):
        try:
            values = keys.split(';')
            returnable_value = self.config
            errored = False
            for value in values:
                if value in returnable_value.keys():
                    returnable_value = returnable_value[value]
                else:
                    errored = True
                    break
            if not errored:
                error = None
                return returnable_value, error
            else:
                returnable_value = json.load(open(self._default_config, 'r'))
                for value in values:
                    if value in returnable_value.keys():
                        returnable_value = returnable_value[value]
                    else:
                        raise Exception
                error = "WAR:: {} key was not present in user config. Value taken from base config".format(keys)
                return returnable_value, error
        except Exception as exc:
            error = 'ERR:: {} key is not present in the configuration file for {}'.format(keys, default_value)
            return default_value, error
    """END"""

