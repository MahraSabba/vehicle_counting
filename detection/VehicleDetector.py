import os
import cv2
import numpy as np

import torch
import torchvision
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor


TOTAL_LABEL = [
    '__background__', 'person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus',
    'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'N/A', 'stop sign',
    'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow',
    'elephant', 'bear', 'zebra', 'giraffe', 'N/A', 'backpack', 'umbrella', 'N/A', 'N/A',
    'handbag', 'tie', 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
    'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard', 'tennis racket',
    'bottle', 'N/A', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl',
    'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
    'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed', 'N/A', 'dining table',
    'N/A', 'N/A', 'toilet', 'N/A', 'tv', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone',
    'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'N/A', 'book',
    'clock', 'vase', 'scissors', 'teddy bear', 'hair drier', 'toothbrush']

VEHICLE_LABEL = ['__background__', 'vehicle']


class VehicleDetector:

    def __init__(self, model=None):
        self.model = model if model is not None else self.create_model()
        self.keypoint_labels = TOTAL_LABEL

    def create_model(self):
        model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)
        model.cuda()
        model.eval()
        return model

    @staticmethod
    def get_model(num_classes):
        model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)
        for param in model.parameters():
            param.requires_grad = False
        in_features = model.roi_heads.box_predictor.cls_score.in_features
        model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)
        model.load_state_dict(
            torch.load(os.path.join(os.path.abspath(os.path.dirname(__file__)), "vehicle_detector.pth")))
        return model

    def convert_to_np_array(self, tensor):
        npimage = tensor
        if isinstance(tensor, torch.cuda.FloatTensor):
            tensor = tensor.mul(255).byte()
        if torch.is_tensor(tensor):
            npimage = np.squeeze(np.transpose(tensor.numpy(), (1, 2, 0)))
        return npimage

    def convert_to_tensor(self, image):
        frame = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = torch.from_numpy(frame.transpose((2, 0, 1)))
        if isinstance(image, torch.ByteTensor) or image.max() > 1:
            return image.float().div(255).cuda()
        return image.cuda()

    def process_frame(self, frame):
        frame = self.convert_to_tensor(frame.copy())
        returned_data = self.model([frame])
        return returned_data[0]
