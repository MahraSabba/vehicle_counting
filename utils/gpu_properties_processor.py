import json
import os
import subprocess
from math import floor


def extract_gpu_memory(detector_name, base_config=None):
    if base_config is None:
        from configuration import config
        base_config = config.Config()
    if "/" in detector_name:
        detector_name = detector_name.split("/")[-1].split(".")[0]
    elif "\\" in detector_name:
        detector_name = detector_name.split("\\")[-1].split(".")[0]
    process_per_gpu = json.load(open(os.path.join(os.getcwd(), base_config.get_process_per_gpu()), 'r'))
    proc = subprocess.run(['gpustat'], stdout=subprocess.PIPE)
    output = proc.stdout.decode('utf-8')
    output = output.split('\n')
    del output[0]
    del output[-1]
    gpus = len(output)
    total_processes = 0
    totalMemory = 0
    gpu_process_allowance = []
    for iterline, line in enumerate(output):
        data = line.split('|')
        gpu_name_and_number = data[0]
        name = gpu_name_and_number.split(']')[-1].replace(' ', '')
        gpu_no = gpu_name_and_number.split(']')[0].replace('[', '')
        print(name)
        for k, v in process_per_gpu.items():
            if k in name:
                total_processes += v[detector_name]
                gpu_process_allowance.append([name, gpu_no, v[detector_name]])
        memory_capacity = data[len(data) - 2]
        total = int(memory_capacity.replace("MB", "").split('/')[-1].strip())
        totalMemory += total

    return totalMemory, floor(total_processes), gpus, gpu_process_allowance
