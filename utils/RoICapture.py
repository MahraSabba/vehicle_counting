import cv2
import numpy as np

class RoICapture:

    def __init__(self, streams):
        self.streams = streams
        self.rois = [[] for _ in range(0, len(self.streams), 1)]
        self.roi_capture_win = "RoI Capture"

    def capture_roi(self, window_name):
        self.roi_capture_win = window_name
        window = cv2.namedWindow(self.roi_capture_win, cv2.WINDOW_NORMAL)
        for stream_no, stream in enumerate(self.streams):
            while True:
                ret, frame = stream.read()
                height, width = frame.shape[:2]
                self.rois[stream_no] = [[0, 0, int(width), int(height)]]
                break
        cv2.destroyWindow(self.roi_capture_win)
        return np.array(self.rois)
