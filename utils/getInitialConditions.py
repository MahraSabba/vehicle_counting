import os
import pickle


class InitialConditionsParser:
    def __init__(self, config):
        self.config = config
        self.rois_fname = "aois.pkl"
        self.cam_link = "cam_link.pkl"
        self.movement_fname = "movements.pkl"
        self.lines = "counting_lines.pkl"
        self.location = os.path.join(os.getcwd(),
                                     self.config.get_initial_condition_location())
        self.roi_location = os.path.join(self.location, self.rois_fname)
        self.movement_link_location = os.path.join(self.location, self.movement_fname)
        self.cam_link_location = os.path.join(self.location, self.cam_link)
        self.counting_line_location = os.path.join(self.location, self.lines)

    def dump_lines(self, lines):
        try:
            pickle.dump(lines, open(self.counting_line_location, 'wb'))
        except Exception as exc:
            print("Error dumping counting lines to file.")
            print(exc)

    def dump_rois(self, rois):
        try:
            pickle.dump(rois, open(self.roi_location, 'wb'))
        except Exception as exc:
            print("Error dumping rois file. ??")
            print(exc)

    def dump_camera_links(self, camera_links):
        try:
            pickle.dump(camera_links, open(self.cam_link_location, 'wb'))
        except Exception as exc:
            print("Error dumping camera links file. ??")
            print(exc)

    def dump_movement_links(self, movement_links):
        try:
            pickle.dump(movement_links, open(self.movement_link_location, 'wb'))
        except Exception as exc:
            print("Error dumping movement links file. ??")
            print(exc)

    def dump_initial_conditions(self, rois, camera_links, movement_links, lines):
        self.dump_rois(rois)
        self.dump_camera_links(camera_links)
        self.dump_movement_links(movement_links)
        self.dump_lines(lines)

    def load_rois(self):
        rois = None
        if os.path.exists(self.roi_location):
            try:
                rois = pickle.load(open(self.roi_location, 'rb'))
            except Exception as exc:
                print(exc)
        return rois

    def load_cam_links(self):
        camera_links = None
        if os.path.exists(self.cam_link_location):
            try:
                camera_links = pickle.load(open(self.cam_link_location, 'rb'))
            except Exception as exc:
                print(exc)
        return camera_links

    def load_movement_links(self):
        movement_links = None
        if os.path.exists(self.movement_link_location):
            try:
                movement_links = pickle.load(open(self.movement_link_location, 'rb'))
            except Exception as exc:
                print(exc)
        return movement_links

    def load_counting_lines(self):
        counting_lines = None
        if os.path.exists(self.counting_line_location):
            try:
                counting_lines = pickle.load(open(self.counting_line_location, 'rb'))
            except Exception as exc:
                print(exc)
        return counting_lines

    def load_initial_conditions(self):
        rois = self.load_rois()
        camera_links = self.load_cam_links()
        movement_link = self.load_movement_links()
        counting_line = self.load_counting_lines()

        return rois, camera_links, movement_link, counting_line