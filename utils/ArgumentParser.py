from argparse import ArgumentParser


class IPIsNoneException(Exception):
    def __init__(self, message, errors=None):
        super().__init__(message)
        self.errors = errors


class IPFormatIsNoneException(Exception):
    def __init__(self, message, errors=None):
        super().__init__(message)
        self.errors = errors


class NoCamerasException(Exception):
    def __init__(self, message, errors=None):
        super().__init__(message)
        self.errors = errors


class BadUserNameToCameraMapping(Exception):
    def __init__(self, message, errors=None):
        super().__init__(message)
        self.errors = errors


class BadPasswordToCameraMapping(Exception):
    def __init__(self, message, errors=None):
        super().__init__(message)
        self.errors = errors


class BadChannelToCameraMapping(Exception):
    def __init__(self, message, errors=None):
        super().__init__(message)
        self.errors = errors


class BadSubtypeToCameraMapping(Exception):
    def __init__(self, message, errors=None):
        super().__init__(message)
        self.errors = errors


class BadIpToCameraMapping(Exception):
    def __init__(self, message, errors=None):
        super().__init__(message)
        self.errors = errors


class BoxParsingError(Exception):
    def __init__(self, message, errors=None):
        super().__init__(message)
        if errors is None:
            self.errors = "The input for the boxes had errors. While parsing your data, errors were encountered."


class ArgParser(ArgumentParser):

    def __init__(self, video_name):
        super().__init__()
        self.default_format = 'rtsp://{username}:{password}@{ip}/cam/realmonitor?' \
                              'channel={channel}&subtype={subtype}'
        self.data = {"addresses": None,
                     "no_of_cameras": None,
                     "ip": None ,
                     "ipformat": None,
                     "username": None,
                     "password": None,
                     "channel": None,
                     "subtype": None,
                     "boxes": None,
                     "movements": None,
                     "lines": None}
        self.set_inputs(video_name)
        self.use_multi = {"ipformat": True,
                          "username": True,
                          "password": True,
                          "channel": True,
                          "subtype": True,
                          "ip": True}
        self.formatting_keys = ['username', 'password', 'ip', 'channel', 'subtype']

    def set_inputs(self, video_name):
        self.add_argument("-a",
                          "-addresses",
                          dest='addresses',
                          default=f'{video_name}',
                          help="Complete Addresses of the Cameras separated with a ',' ",
                          type=str,
                          required=False)
        self.add_argument("-n",
                          "--camera_number",
                          dest='camera_number',
                          help="Number of Cameras to connect",
                          default=1,
                          type=int,
                          required=False)
        self.add_argument("-i",
                          "--ip",
                          dest='ip',
                          help="IP Address(s) of the Stream",
                          default=None,
                          type=str,
                          required=False)
        self.add_argument('-f',
                          "--format",
                          dest='format',
                          default=self.default_format,
                          help="The format(s) of the address required for the ip camera",
                          type=str,
                          required=False)
        self.add_argument('-u',
                          '--username',
                          dest='username',
                          help='Username(s) of the Camera',
                          default='admin',
                          type=str,
                          required=False)
        self.add_argument('-p',
                          '--password',
                          dest='password',
                          default="admin123",
                          help='Password(s) of the Camera. Do not use \',\' in the password.',
                          type=str,
                          required=False)
        self.add_argument('-c',
                          '--channel',
                          dest='channel',
                          default='2,3',
                          help='Channel(s) of the camera',
                          type=str,
                          required=False)
        self.add_argument('-s',
                          '--subtype',
                          dest='subtype',
                          default='0',
                          help='Subtype(s) of the camera',
                          type=str,
                          required=False)
        self.add_argument('-b',
                          '--boxes',
                          dest='boxes',
                          default=None,
                          help="The x,y,w,h, coordinates of the boxes.",
                          type=str,
                          required=False)
        self.add_argument('-m',
                          '--movements',
                          dest='movements',
                          default=None,
                          help="The x,y,w,h, coordinates of the entry exit areas.",
                          type=str,
                          required=False)
        self.add_argument('-l',
                          '--lines',
                          dest='lines',
                          default=None,
                          help='The x1,y1,x2,y2 of the line',
                          type=str,
                          required=False)

    def parse_arguments(self):
        parsed_args = self.parse_args()
        self.data['addresses'] = parsed_args.addresses
        self.data['no_of_cameras'] = parsed_args.camera_number
        self.data['ip'] = parsed_args.ip
        self.data['ipformat'] = parsed_args.format
        self.data['username'] = parsed_args.username
        self.data['password'] = parsed_args.password
        self.data['channel'] = parsed_args.channel
        self.data['subtype'] = parsed_args.subtype
        self.data['boxes'] = parsed_args.boxes
        self.data['movements'] = parsed_args.movements
        self.data['lines'] = parsed_args.lines

    def sanitize_arguments(self):

        if self.data['addresses'] is not None:
            if isinstance(self.data['addresses'], str):
                if "," in self.data['addresses']:
                    return False
                else:
                    return True

        if self.data['ip'] is None:
            raise IPIsNoneException("Entered IP address is None or has not been entered.")

        if self.data['ipformat'] is None:
            raise IPFormatIsNoneException("IP address has been overwritten by your input and set to None.")

        if self.data['username'] is None:
            raise BadUserNameToCameraMapping("Username is not given.")

        if self.data['password'] is None:
            raise BadPasswordToCameraMapping("Password is not given.")

        if self.data['channel'] is None:
            raise BadChannelToCameraMapping("Channel is not given.")

        if self.data['subtype'] is None:
            raise BadSubtypeToCameraMapping("Subtype is not given.")

        no_of_cameras = self.data['no_of_cameras']
        if not isinstance(no_of_cameras, int):
            raise NoCamerasException("You have not entered the number of camera feeds to be processed")
        elif no_of_cameras < 1:
            raise NoCamerasException("No of cameras cannot be less than 1")
        elif no_of_cameras == 1:
            return no_of_cameras

        self.data['username'] = self.split_data(self.data['username'])
        self.data['password'] = self.split_data(self.data['password'])
        self.data['ip'] = self.split_data(self.data['ip'])
        self.data['channel'] = self.split_data(self.data['channel'])
        self.data['subtype'] = self.split_data(self.data['subtype'])
        self.data['ipformat'] = self.split_data(self.data['ipformat'])

        self.verify_equivalence("username",
                                BadUserNameToCameraMapping,
                                "Usernames are {}. Cameras are {}",
                                str,
                                no_of_cameras)

        self.verify_equivalence("password",
                                BadPasswordToCameraMapping,
                                "Passwords are {}. Cameras are {}",
                                str,
                                no_of_cameras)

        self.verify_equivalence("channel",
                                BadChannelToCameraMapping,
                                "Channels are {}. Cameras are {}",
                                str,
                                no_of_cameras)
        self.verify_equivalence("subtype",
                                BadSubtypeToCameraMapping,
                                "Subtypes are {}. Cameras are {}",
                                str,
                                no_of_cameras)

        self.verify_equivalence("ip",
                                BadIpToCameraMapping,
                                "Ips are {}. Cameras are {}",
                                str,
                                no_of_cameras)
        self.verify_equivalence("ipformat",
                                BadIpToCameraMapping,
                                "IP Formats are {}. Cameras are {}",
                                str,
                                no_of_cameras)

        return no_of_cameras

    @staticmethod
    def split_data(data):
        if ',' in data:
            return [x.strip() for x in data.split(',')]
        return data

    def verify_equivalence(self, key, exceptiontype, exceptionstring, valuetype, no_of_cameras):
        if isinstance(self.data[key], valuetype):
            self.use_multi[key] = False
        elif isinstance(self.data[key], list):
            no_of_keys = len(self.data[key])
            if no_of_keys == no_of_cameras:
                self.use_multi[key] = True
            else:
                raise exceptiontype(exceptionstring.format(no_of_keys, no_of_cameras))

    def add_data(self, key, camera_no):
        if self.use_multi[key]:
            return self.data[key][camera_no]
        else:
            return self.data[key]

    def create_address(self, camera):
        address = self.add_data('ipformat', camera)
        formattings = {}
        for key in self.formatting_keys:
            formattings[key] = self.add_data(key, camera)
        return address.format(**formattings)

    def get_formatted_address(self):
        self.parse_arguments()
        no_of_cameras = self.sanitize_arguments()
        if isinstance(no_of_cameras, bool):
            if no_of_cameras:
                return self.data['addresses'].strip()
            else:
                return [address.strip() for address in self.data['addresses'].split(',')]

        if no_of_cameras == 1:
            address = self.data['ipformat']
            complete_address = address.format(username=self.data['username'],
                                              password=self.data['password'],
                                              ip=self.data['ip'],
                                              channel=self.data['channel'],
                                              subtype=self.data['subtype'])
            return complete_address
        else:
            addresses = []
            for camera in range(0, no_of_cameras, 1):
                addresses.append(self.create_address(camera))
            return addresses

    @staticmethod
    def save_to_roi_box(roi, cur_camera_rois):
        try:
            if roi == "":
                raise BoxParsingError("RoI defined by user is null")
            x, y, w, h = roi.split('-')
            cur_camera_rois.append([x, y, w, h])
        except BoxParsingError as exc:
            pass
        return cur_camera_rois

    @staticmethod
    def save_to_line(line, cur_camera_lines):
        try:
            if line == "":
                raise BoxParsingError("Line define by the user is null")
            x1, y1, x2, y2 = line.split('-')
            cur_camera_lines.append([x1, y1, x2, y2])
        except BoxParsingError as exc:
            pass
        return cur_camera_lines

    def get_and_validate_rois_from_user(self, boxes):
        no_of_cameras = self.data['no_of_cameras']
        stream_rois = []

        if boxes is None:
            return None

        if ',' not in boxes:
            if no_of_cameras == 1:
                cur_camera_rois = []
                for roi in boxes.split(';'):
                    cur_camera_rois = self.save_to_roi_box(roi, cur_camera_rois)
                return stream_rois
            return None

        cur_camera_rois = []
        for roi_list in boxes.split(','):
            if roi_list == '':
                raise BoxParsingError("RoI defined by user is null")
            if ";" not in roi_list:
                cur_camera_rois = self.save_to_roi_box(roi_list, cur_camera_rois)
                continue

            rois = []
            for roi in roi_list.split(';'):
                rois = self.save_to_roi_box(roi, rois)
            cur_camera_rois.append(rois)
        return cur_camera_rois

    def get_and_validate_lines_from_users(self):
        no_of_cameras = self.data['no_of_cameras']
        lines = self.data['lines']
        stream_rois = []
        if lines is None:
            return None

        cur_camera_lines = []
        if ',' not in lines:
            if no_of_cameras == 1:
                for line in lines.split(';'):
                    cur_camera_lines = self.save_to_line(line, cur_camera_lines)
                return cur_camera_lines
            return None

        for line_list in lines.split(';'):
            if line_list == '':
                raise BoxParsingError('Line defined by the user is null')
            if ";" not in line_list:
                cur_camera_lines = self.save_to_line(line_list, cur_camera_lines)
                continue
            cur_lines = []
            for line in lines.split(';'):
                cur_lines = self.save_to_line(line, cur_lines)
            cur_camera_lines.append(cur_lines)
        return cur_camera_lines

