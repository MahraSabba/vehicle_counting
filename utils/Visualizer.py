import numpy as np
import cv2
from base64 import b64encode
from hashlib import sha256


TOTAL_LABEL = [
    '__background__', 'person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus',
    'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'N/A', 'stop sign',
    'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow',
    'elephant', 'bear', 'zebra', 'giraffe', 'N/A', 'backpack', 'umbrella', 'N/A', 'N/A',
    'handbag', 'tie', 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
    'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard', 'tennis racket',
    'bottle', 'N/A', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl',
    'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
    'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed', 'N/A', 'dining table',
    'N/A', 'N/A', 'toilet', 'N/A', 'tv', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone',
    'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'N/A', 'book',
    'clock', 'vase', 'scissors', 'teddy bear', 'hair drier', 'toothbrush']


class Visualizer:

    def __init__(self, labels):
        self.box_color = (255, 0, 0)
        self.tracked_object_box_color = (255, 255, 0)
        self.kp_line_color = (0, 255, 0)
        self.kp_line_width = 3
        self.font = cv2.FONT_HERSHEY_SIMPLEX
        self.line_type = cv2.LINE_AA
        self.font_size = 1
        self.box_line_width = 1
        self.labels = labels
        self.aoi_color = (125, 125, 255)
        self.maoi_color = (255, 112, 95)
        self.area_line_width = 1

    def display_flow_points(self,
                            prediction,
                            image,
                            showpoints=True):
        if prediction is None:
            return image
        for point in prediction:
            cv2.circle(image, (point[0], point[1]), 5, (0, 0, 255), thickness=5, lineType=8)
        return image

    def display_prediction(self,
                           prediction,
                           image,
                           showboxes=True,
                           score_threshold=0.60):
        labels = prediction['labels']
        boxes = prediction['boxes']
        scores = prediction['scores']
        labels = labels.cpu().detach().numpy()
        boxes = boxes.cpu().detach().numpy()
        scores = scores.cpu().detach().numpy()
        for i in range(labels.shape[0]):
            if scores[i] > score_threshold:
                if showboxes:
                    x1, y1, x2, y2 = boxes[i]
                    label = labels[i]
                    cv2.rectangle(image, (int(x1), int(y1)),
                                  (int(x2), int(y2)),
                                  self.box_color,
                                  self.box_line_width)
                    cv2.putText(image, "{} - {:02f}".format(TOTAL_LABEL[label], scores[i]),
                                (x1, y1),
                                self.font,
                                self.font_size,
                                self.box_color,
                                self.box_line_width,
                                self.line_type)
                    cv2.putText(image, "{:.2f}".format(scores[i]),
                                (x1, y1),
                                self.font,
                                self.font_size,
                                self.box_color,
                                self.box_line_width,
                                self.line_type)
        return image

    def display_tracked_objects(self, predictions, state, image, showboxes=True, show_id=True):
        pred_shape = predictions.shape
        for row_id, row in enumerate(predictions):
            try:
                x1, y1, x2, y2, tracking_id = row
            except Exception as exc:
                x1, y1, x2, y2, tracking_id, object_class = row

            if state[row_id] == "aoi":
                color = self.aoi_color
            elif state[row_id] == "maoi":
                color = self.maoi_color
            else:
                color = self.tracked_object_box_color

            if showboxes:
                cv2.rectangle(image,
                              (int(x1), int(y1)),
                              (int(x2), int(y2)),
                              color,
                              self.box_line_width)
            if show_id:
                cv2.putText(image, "{}".format(str(sha256(tracking_id).hexdigest())[:8]),
                            (x1, y1),
                            self.font,
                            self.font_size,
                            color,
                            self.box_line_width,
                            self.line_type)
        return image

    def display_area(self, areas, image, type=None):
        if type == 'aoi':
            area_color = self.aoi_color
        elif type == 'maoi':
            area_color = self.maoi_color
        for area in areas:
            cv2.rectangle(image,
                          (int(area[0]), int(area[1])),
                          (int(area[2]), int(area[3])),
                          area_color,
                          self.area_line_width)
        return image
