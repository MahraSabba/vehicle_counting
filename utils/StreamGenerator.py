import cv2


class NoStreamFoundException(Exception):
    def __init__(self, message, errors=None):
        super().__init__(message)
        self.errors = errors


class StreamGenerator:

    def __init__(self, camera_links):
        self.stream_links = camera_links
        self.stream_urls = []
        self.streams = []
        self.create_streams()

    def get_streams(self):
        if self.streams:
            return len(self.streams), self.streams, self.stream_urls

    def clear_streams(self):
        if self.streams:
            for stream in self.streams:
                stream.close()
            self.streams = []

    def create_streams(self):
        if isinstance(self.stream_links, str):
            stream = self.initialize_stream(self.stream_links)
            if isinstance(stream, cv2.VideoCapture):
                self.streams.append(stream)
                self.stream_urls.append(self.stream_links)

        elif isinstance(self.stream_links, list):
            for link in self.stream_links:
                if not isinstance(link, str):
                    continue
                stream = self.initialize_stream(link)
                if not isinstance(stream, cv2.VideoCapture):
                    continue
                self.streams.append(stream)
                self.stream_urls.append(link)
        else:
            raise NoStreamFoundException("No valid streams were found in the streaming urls given or created")

    def verify(self, stream):
        if stream.isOpened():
            ret, frame = stream.read()
            if ret is not None:
                return True
        return False

    def initialize_stream(self, stream_url):
        stream = cv2.VideoCapture(stream_url)
        if self.verify(stream):
            return stream
        return None
