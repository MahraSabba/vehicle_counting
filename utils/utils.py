import numpy as np


def iou(boxes, threshold=0.6):
    boxes[:, 2:] = boxes[:, :2] + boxes[:, 2:]
    box_nums = boxes.shape[0]
    matches = []
    for i in range(box_nums):
        current_box = boxes[i, :]
        cur_box_area = current_box[2:].prod()
        comparative_boxes = boxes[i:, :]
        overlapped_box = np.c_[np.maximum(current_box[0], comparative_boxes[:, 0])[:, np.newaxis],
                               np.maximum(current_box[1], comparative_boxes[:, 1])[:, np.newaxis],
                               np.minimum(current_box[2], comparative_boxes[:, 2])[:, np.newaxis],
                               np.minimum(current_box[3], comparative_boxes[:, 3])[:, np.newaxis]]
        wh = np.maximum(0., overlapped_box[:, 2:]-overlapped_box[:, :2])

        area_of_intersection = wh.prod(axis=1)
        area_comparative_box = comparative_boxes[:, 2:].prod(axis=1)
        current_iou = area_of_intersection/(cur_box_area + area_comparative_box - area_of_intersection)
        current_iou[current_iou < threshold] = 0
        max_match = np.argmax(current_iou, axis=0)
        matches.append((i, max_match))
    return matches


def join_boxes(main_box, secondary_box):
    """Expecting x1, y1, x2, y2"""
    main_box[0] = main_box[0] if main_box[0] < secondary_box[0] else secondary_box[0]
    main_box[1] = main_box[1] if main_box[1] < secondary_box[1] else secondary_box[1]
    main_box[2] = main_box[2] if main_box[2] > secondary_box[2] else secondary_box[2]
    main_box[3] = main_box[3] if main_box[3] > secondary_box[3] else secondary_box[3]
    return main_box


def combine_prediction_overlapping_pairs(predictions, pairs="1,4", threshold=0.69):
    pairs = [[int(label_id) for label_id in label_pairs.split(',')] for label_pairs in pairs.split(';')]
    labels, boxes_xyxy, boxes, scores = predictions
    matches = iou(boxes.copy(), threshold=threshold)
    deletable_box_ids = []
    for box_i_id, box_j_id in matches:
        if box_j_id == 0:
            continue
        for pair in pairs:
            if labels[box_i_id] in pair and labels[box_j_id] in pair:
                main_id, secondary_id = (box_i_id, box_j_id) \
                    if labels[box_i_id] == pair[0] else \
                    (box_j_id, box_i_id)
                boxes_xyxy[main_id] = join_boxes(boxes_xyxy[main_id], boxes_xyxy[secondary_id])
                deletable_box_ids.append(secondary_id)
    print(deletable_box_ids)
    deletable_box_ids = sorted(list(set(deletable_box_ids)), reverse=True)
    for del_ids in deletable_box_ids:
        labels = np.delete(labels, del_ids)
        boxes_xyxy = np.delete(boxes_xyxy, del_ids, axis=0)
        scores = np.delete(scores, del_ids)
    return labels, boxes_xyxy, scores


def threshold_detections(predictions, threshold=0.6):
    labels = predictions['labels']
    boxes = predictions['boxes']
    scores = predictions['scores']

    labels = labels.cpu().detach().numpy()
    boxes = boxes.cpu().detach().numpy()
    scores = scores.cpu().detach().numpy()

    deletable_rows = []

    for i in range(labels.shape[0]):
        if labels[i] not in [1, 3, 4, 6, 8]:
            deletable_rows.append(i)
        if scores[i] < threshold:
            deletable_rows.append(i)

    reversed_row = list(sorted(deletable_rows, reverse=True))

    labels = np.delete(labels, reversed_row, axis=0)
    boxes = np.delete(boxes, reversed_row, axis=0)
    scores = np.delete(scores, reversed_row, axis=0)
    boxes_xywh = np.hstack([boxes[:, :2], boxes[:, 2:] - boxes[:, :2]])
    labels, boxes, scores = combine_prediction_overlapping_pairs((labels, boxes, boxes_xywh, scores))
    return labels, boxes, scores


def normalize_boxes(boxes, normalizer):
    boxes = boxes/normalizer
    return boxes


