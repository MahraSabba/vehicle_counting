import cv2


class LineCapture:

    def __init__(self, streams):
        self.streams = streams
        self.lines = [[[]] for _ in range(0, len(self.streams), 1)]
        self.lines_capture_name = "Counting Lines Definer"
        self.current_stream = None

    def draw_lines(self, stream_no, image):
        if not self.lines[stream_no]:
            return
        for line in self.lines[stream_no]:
            if not line:
                pass
            if len(line) == 1:
                cv2.circle(image, (line[0][0], line[0][1]), 15, (0, 255, 0), 15)
            elif len(line) == 2:
                cv2.line(image, (line[0][0], line[0][1]), (line[1][0], line[1][1]), (0, 255, 0), 1)
        return image

    def set_unset_point(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            current_lines = self.lines[self.current_stream]
            setted = False
            for line_no, line in enumerate(current_lines):
                if len(line) >= 2:
                    pass
                else:
                    setted = True
                    self.lines[self.current_stream][line_no].append([x, y])
                    break
            if not setted:
                self.lines[self.current_stream].append([[x, y]])
        elif event == cv2.EVENT_RBUTTONDOWN:
            if len(self.lines[self.current_stream]) > 1:
                del self.lines[self.current_stream][-1]
            else:
                self.lines[self.current_stream]=[[]]

    def capture_lines(self, window_name):
        self.lines_capture_name = window_name
        # window = cv2.namedWindow(self.lines_capture_name, cv2.WINDOW_NORMAL)
        # cv2.setMouseCallback(self.lines_capture_name, self.set_unset_point)
        for stream_no, stream in enumerate(self.streams):
            self.current_stream = stream_no
            while True:
                ret, frame = stream.read()
                height, width = frame.shape[:2]
                self.lines[stream_no] = [
                    [[int(width/2), 0], [int(width/2), int(height)]]
                ]
                break
        # cv2.destroyWindow(self.lines_capture_name)
        return self.lines
