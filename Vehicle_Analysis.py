import os
import ray
from configuration.config import Config
from utils.getInitialConditions import InitialConditionsParser
from utils.ArgumentParser import ArgParser
from utils.StreamGenerator import StreamGenerator
from utils.RoICapture import RoICapture
from utils.LineCapture import LineCapture
from app.worker import StreamWorker
from time import sleep
from os import kill


config = Config()
ray.init(temp_dir=config.config['ray']['log_dir'],
         memory=config.config['ray']['memory'],
         object_store_memory=config.config['ray']['object_store_memory'],
         num_gpus=config.config['ray']['num_gpus'],
         num_cpus=config.config['ray']['num_cpus'])


class Analyzer:
    def __init__(self, config, video_name):
        self.data = None
        self.workers = []
        self.config = config
        self.ic_parser = InitialConditionsParser(self.config)
        self.start_analysis(video_name)
        self.process_initializer()

    def start_analysis(self, video_name):
        self.data = self.get_initial_data(video_name)

    def parse_arguments(self, video_name):
        parser = ArgParser(video_name)
        return parser.get_formatted_address(), \
               parser.get_and_validate_rois_from_user(parser.data['boxes']),\
               parser.get_and_validate_rois_from_user(parser.data['movements']), \
               parser.get_and_validate_lines_from_users()

    def get_initial_data(self, video_name):
        """
        Camera Links: This can be rtsp video stream urls or video files in a local storage location.

        Rois: These are regions of interests where additional analysis needs to be performed.

        Movement Areas: These are entry exit areas where objects can appear and disappear.
        :return:
        Data: A list of tuples containing camera link, roi and movement area for each video.
        """
        camera_links, rois, movement_area, lines = self.parse_arguments(video_name)

        prev_rois, \
        prev_camera_links, \
        prev_camera_movement_links, \
        prev_counting_lines = self.ic_parser.load_initial_conditions()

        if prev_rois is not None and prev_camera_links is not None:
            if prev_camera_links == camera_links:
                if rois is None:
                    rois = prev_rois
                if movement_area is None:
                    movement_area = prev_camera_movement_links
                if lines is None:
                    lines = prev_counting_lines

        streamgen = StreamGenerator(camera_links)
        stream_no, streams, stream_urls = streamgen.get_streams()

        if rois is None:
            roigen = RoICapture(streams)
            # List of (x, y, w, h) for each stream
            rois = roigen.capture_roi("Define your Area of Interest in the current View")
            for roi in rois:
                roi[:, 2] = roi[:, 2] + roi[:, 0]
                roi[:, 3] = roi[:, 3] + roi[:, 1]

        if movement_area is None:
            roigen = RoICapture(streams)
            movement_area = roigen.capture_roi("Define your Entry Exit Areas in the current View")
            for maoi in movement_area:
                maoi[:, 2] = maoi[:, 2] + maoi[:, 0]
                maoi[:, 3] = maoi[:, 3] + maoi[:, 1]

        if lines is None:
            linesgen = LineCapture(streams)
            lines = linesgen.capture_lines("Define lines to start counting")

        self.ic_parser.dump_initial_conditions(rois, camera_links, movement_area, lines)
        data = []
        for str_no, stream in enumerate(streams):
            data.append(
                [rois[str_no],
                 stream_urls[str_no],
                 movement_area[str_no],
                 lines[str_no]
                 ])
            stream.release()
        return data

    def process_initializer(self):
        for data_iter, (rois, stream_urls, movement_areas, lines) in enumerate(self.data):
            actor = StreamWorker.remote(rois, stream_urls, movement_areas, lines, data_iter+1)
            self.workers.append(actor)

        # The line below makes the main thread wait for the child processes to complete or exit.
        # If the main thread exits, the child processes are automatically killed.
        results = ray.get([worker.wait_command.remote() for worker in self.workers])
        self.workers = []


if __name__ == '__main__':
    folder_location = "/home/trafficgenius/TrafficGenius/Ve/hicleDatasetMaker/data/videos"
    output_location = "/home/trafficgenius/TrafficGenius/VehicleDatasetMaker/data/dataset"
    out_files = [out_file.split('.')[0] for out_file in os.listdir(output_location) if '.csv' in out_file]
    video_locations = [os.path.join(folder_location, video_name) for video_name in os.listdir(folder_location) if
                       video_name.split('.')[0] not in out_files]
    for video_loc in video_locations:
        analyzer = Analyzer(config, video_loc)
        analyzer = None
        sleep(10)
