from os import getcwd
import cv2
import time
import json
import numpy as np

from utils import utils
from configuration import config
from utils.Visualizer import Visualizer

from detection.VehicleDetector import VehicleDetector
from tracking.base_tracker import SimilarityTracker
from tracking.flow_tracker.flow_tracking import FlowTracker


class StreamWorker(object):
    def __init__(self, rois, stream_url, movement_areas, chain):
        self.name = "{}".format(time.time())
        self.stream_url = stream_url
        self.chain = chain
        self.stream = None
        self.playing = True
        self.cur_frame = 0
        self.frame_skip = 1
        self.base_config = config.Config()
        self.labels = json.load(open(self.base_config.get_labels_file(), 'r'))
        self.detector = VehicleDetector()
        self.aois = rois
        self.maois = movement_areas
        self.visualizer = Visualizer(labels={v: k for k, v in self.labels.items()})
        self.tracker = SimilarityTracker(base_dir=getcwd(),
                                         base_config=self.base_config,
                                         labels=self.labels,
                                         aois=rois,
                                         maois=movement_areas)
        self.flowtracker = FlowTracker()
        self.tracker.set_chain(chain)
        self.window = cv2.namedWindow(self.name, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(self.name, (600, 400))
        self.keep_trying_to_play_seconds = 30
        self.not_returned_started = False
        self.start()

    def start(self):
        self.counter = None
        print("{}".format(self.stream_url))

        self.stream = cv2.VideoCapture(self.stream_url)
        width, height = self.stream.get(cv2.CAP_PROP_FRAME_WIDTH), self.stream.get(cv2.CAP_PROP_FRAME_HEIGHT)
        normalizer = np.array([width, height, width, height])
        while self.stream.isOpened():
            ret, frame = self.stream.read()
            self.cur_frame += 1
            if ret and self.cur_frame % self.frame_skip == 0:
                start_time = time.time()
                processable_frame = frame.copy()
                detections = self.detector.process_frame(processable_frame)
                labels, boxes, scores = utils.threshold_detections(detections)
                boxes = utils.normalize_boxes(boxes, normalizer)
                tracked_detections, state = self.tracker.update(processable_frame,
                                                                self.cur_frame,
                                                                boxes,
                                                                labels,
                                                                scores,
                                                                time.time())
                cur_points = self.flowtracker.update(frame.copy(), tracked_detections)
                frame = self.visualizer.display_area(self.aois, frame, 'aoi')
                frame = self.visualizer.display_area(self.maois, frame, 'maoi')
                # frame = self.visualizer.display_prediction(detections, frame)
                frame = self.visualizer.display_tracked_objects(tracked_detections, state, frame)
                frame = self.visualizer.display_flow_points(cur_points, frame)
                print(time.time() - start_time)
                cv2.imshow(self.name, frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    self.playing = False
                    break
            if not ret:
                if not self.not_returned_started:
                    self.not_returned_started = True
                    self.counter = time.time()
                if self.not_returned_started:
                    diff = time.time() - self.counter
                    if diff > self.keep_trying_to_play_seconds:
                        self.playing = False
                        break


if __name__ == '__main__':
    StreamWorker(np.array([[0, 0, 400, 400]]),
                 "/home/trafficgenius/TrafficGenius/Detection_Cloud_System/automated_vehicle_system/nha/2019_1205_104504_001.MOV",
                 np.array([[0, 0, 1920, 1080]]),
                 1)
