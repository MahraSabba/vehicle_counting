from datetime import time
from datetime import datetime
from hashlib import sha3_224
from base64 import b64encode
from tracking.dense_tracking.LineOfCount import LineOfCount


def set_line(line):
    return LineOfCount(line)


class Counter(object):
    def __init__(self, lines):
        self._count = []
        self._lines = []
        self.count_data = {}
        if lines is not None:
            for line in lines:
                self._lines.append(set_line(line))
                self._count.append(0)

    def count_objects(self, tracked_objects):
        for tobj in tracked_objects:
            self.count(tobj)
        print(self._count)

    def count(self, tracker):
        if tracker.counted:
            return
        if tracker.correct_id == 0:
            return
        for line_no, line in enumerate(self._lines):
            if line.has_crossed_counting_line(tracker.bbox):

                tracker.counted = True
                self._count[line_no] += 1
                self.count_data[datetime.now().strftime('%H%M%S%f')] = {
                    'object_id': str(tracker.correct_id),
                    'category': str(tracker.max_class_name),
                    'count': str(self._count),
                    'line': str(line_no)}
                return
