# vim: expandtab:ts=4:sw=4
from __future__ import absolute_import
import os
import pickle
import numpy as np

from . import kalman_filter
from . import linear_assignment
from . import iou_matching
from .track import Track, TrackState

from ..dense_tracking.AreaOfInterests import AreaOfInterests
from ..dense_tracking.MovementAreas import MovementAreas
from tracking.counter import Counter


class Tracker:
    """
    This is the multi-target tracker.

    Parameters
    ----------
    metric : nn_matching.NearestNeighborDistanceMetric
        A distance metric for measurement-to-track association.
    max_age : int
        Maximum number of missed misses before a track is deleted.
    n_init : int
        Number of consecutive detections before the track is confirmed. The
        track state is set to `Deleted` if a miss occurs within the first
        `n_init` frames.

    Attributes
    ----------
    metric : nn_matching.NearestNeighborDistanceMetric
        The distance metric used for measurement to track association.
    max_age : int
        Maximum number of missed misses before a track is deleted.
    n_init : int
        Number of frames that a track remains in initialization phase.
    kf : kalman_filter.KalmanFilter
        A Kalman filter to filter target trajectories in image space.
    tracked_objects : List[Track]
        The list of active tracks at the current time step.

    """

    def __init__(self, base_path, metric, max_iou_distance=0.7, max_age=30, n_init=2, base_config=None, aois=None,
                 maois=None, clines=None, max_ma_age=5):
        """

        :param base_path: Base path of the whole program so that images of the tracked objects can be saved accordingly.
        :param metric: The distance matching algorithm between dets and tracks. Currently set to cosine distance.
        :param max_iou_distance: Max iou 
        :param max_age: Max age in the number of cycles (cascade depth in the matching cascade function in the 
                        linear assignment function.) It is also the max frame that the tracker can exist for while it
                        is not being matched 
        :param n_init: 
        """
        if base_config is None:
            from configuration import config
            base_config = config.Config()
        self.base_config = base_config
        self.base_path = base_path

        self.metric = metric
        self.max_iou_distance = max_iou_distance
        self.max_age = max_age
        self.max_ma_age = max_ma_age
        self.n_init = n_init
        self.kf = kalman_filter.KalmanFilter()
        self.max_dead_tracker_limit = base_config.get_tsim_max_dead_tracker_limit()
        self._next_id = 1
        self.tracking_id = 0
        self.frame_count = 0
        self.cur_time = 0
        self.exported = False
        self.chain = None
        self.aois = AreaOfInterests(aois)
        self.maois = MovementAreas(maois)
        self.counter = Counter(clines)
        self.tracked_objects = []
        self.dead_trackers = []
        self.dead_ids = []

    def create_dataset_file(self, name):
        self.dataset_file_name = os.path.join(self.base_path, 'data', 'dataset', f'{name}.csv')
        open(self.dataset_file_name, 'w')
        self.image_dir_name = os.path.join(self.base_path, "data", "dataset", f'{name}')
        if not os.path.exists(self.image_dir_name):
            os.mkdir(self.image_dir_name)


    @staticmethod
    def initialize_track_dict(category_names_dict):
        Track.all_class_name = category_names_dict

    def set_chain(self, chain):
        self.chain = chain

    def predict(self):
        """Propagate track state distributions one time step forward.

        This function should be called once every time step, before `update`.
        """
        for track in self.tracked_objects:
            track.predict(self.kf)

    def update(self, detections, cur_frame_image, current_frame_number, current_time):
        """Perform measurement update and track management.

        Parameters
        ----------
        detections : List[deep_sort.detection.Detection]
            A list of detections at the current time step.
        """

        self.frame_count += 1
        self.cur_time = current_time

        # Run matching cascade.
        matches, unmatched_tracks, unmatched_detections = self._match(detections)

        # Update track set i.e. update the tracked object which has been currently matched.
        for track_idx, detection_idx in matches:
            self.tracked_objects[track_idx].update(self.kf,
                                                   detections[detection_idx],
                                                   cur_frame_image,
                                                   current_frame_number,
                                                   current_time)
        self.aois.update([self.tracked_objects[i] for i, _ in matches])
        self.maois.update([self.tracked_objects[i] for i, _ in matches])
        # Check up on the tracker. If it is in a tentative state, set up for deletion, else
        # check if the time since last update has exceeded the limit set and delete if it has.
        for track_idx in unmatched_tracks:
            self.tracked_objects[track_idx].mark_missed()

        # Create tracked object.
        if len(unmatched_detections) != 0:
            unmatch_detections = [detections[detection_id] for detection_id in unmatched_detections]
            """Returns a list of ids of detections inside the maois"""
            detections_in_maoi = self.maois.update(unmatch_detections, type='detection')
            for unmatched_detection_id, unmatched_detection in enumerate(detections_in_maoi):
                self._initiate_track(unmatched_detection,
                                     current_frame_number,
                                     current_time,
                                     cur_frame_image)

        # Remove tracked objects set for deletion
        for tracked_object in self.tracked_objects:
            if tracked_object.is_deleted and tracked_object.correct_id != 0:
                if tracked_object.correct_id not in self.dead_ids:
                    self.dead_trackers.append(tracked_object)
                    self.dead_ids.append(tracked_object.correct_id)

        # Save image and delete tracker
        while len(self.dead_trackers) > self.max_dead_tracker_limit:
            del self.dead_trackers[0]

        # Remove tracked objects from tracking list
        max_range = len(self.tracked_objects)-1
        for i in range(max_range, 0, -1):
            if self.tracked_objects[i].is_deleted():
                self.tracked_objects[i].save_data_to_file(self.dataset_file_name, self.image_dir_name)
                self.tracked_objects.pop(i)

        # self.tracked_objects = [t for t in self.tracked_objects if not t.is_deleted()]
        self.counter.count_objects([tracker for tracker in self.tracked_objects if not tracker.is_deleted()])

        # Update distance metric.
        active_targets = [t.track_id for t in self.tracked_objects if t.is_confirmed()]
        features, targets = [], []
        for track in self.tracked_objects:
            if not track.is_confirmed():
                continue
            # Set the tracking id for the tracked object if it is confirmed.
            if track.correct_id == 0:
                self.tracking_id += 1
                track.correct_id = self.tracking_id
            features += track.features
            targets += [track.track_id for _ in track.features]
            track.features = []
        self.metric.partial_fit(np.asarray(features), np.asarray(targets), active_targets)

    def _match(self, detections):
        """
            This specific function finds the distance between the new detection features and
            the old tracked detections.
        """
        def gated_metric(tracks, dets, track_indices, detection_indices):
            features = np.array([dets[i].feature for i in detection_indices])
            targets = np.array([tracks[i].track_id for i in track_indices])
            cost_matrix = self.metric.distance(features, targets)
            cost_matrix = linear_assignment.gate_cost_matrix(
                self.kf, cost_matrix, tracks, dets, track_indices,
                detection_indices)
            return cost_matrix

        # Split track set into confirmed and unconfirmed tracks.
        confirmed_tracks = []
        unconfirmed_tracks = []
        for i, t in enumerate(self.tracked_objects):
            if t.is_confirmed():
                confirmed_tracks.append(i)
            else:
                unconfirmed_tracks.append(i)

        # Associate confirmed tracks using appearance features.
        matches_a, unmatched_tracks_a, unmatched_detections = \
            linear_assignment.matching_cascade(
                gated_metric, self.metric.matching_threshold, self.max_age,
                self.tracked_objects, detections, confirmed_tracks)

        # Associate remaining tracks together with unconfirmed tracks using IOU.
        iou_track_candidates = unconfirmed_tracks + [
            k for k in unmatched_tracks_a if
            self.tracked_objects[k].time_since_update == 1]

        unmatched_tracks_a = [
            k for k in unmatched_tracks_a if
            self.tracked_objects[k].time_since_update != 1]

        matches_b, unmatched_tracks_b, unmatched_detections = \
            linear_assignment.min_cost_matching(
                iou_matching.iou_cost, self.max_iou_distance, self.tracked_objects,
                detections, iou_track_candidates, unmatched_detections)

        # Combined and return
        matches = matches_a + matches_b
        unmatched_tracks = list(set(unmatched_tracks_a + unmatched_tracks_b))
        return matches, unmatched_tracks, unmatched_detections

    def _initiate_track(self, detection,
                        current_frame_number,
                        current_time,
                        cur_frame_image):

        mean, covariance = self.kf.initiate(detection.to_xyah())

        if len(self.dead_trackers) > 0:
            if self._check_with_dead_trackers(detection, cur_frame_image, current_frame_number, current_time):
                return

        self.tracked_objects.append(Track(
            mean, covariance, self._next_id, self.n_init, self.max_age,
            detection.feature, detection.to_tlbr(), cur_frame_image,
            current_frame_number, current_time, self.chain, detection.confidence,
            detection.category_name, self.base_path, self.base_config))
        self._next_id += 1

    def _check_with_dead_trackers(self, detection, image, cur_frame, cur_time):
        for tracker_id, dead_tracker in enumerate(self.dead_trackers):
            this_tracker_result = []
            if detection.category_name == dead_tracker.max_class_name:
                this_tracker_result.append(True)
            else:
                this_tracker_result.append(False)
                continue

            iou = iou_matching.iou_tlbr(dead_tracker.bbox, detection.to_tlbr())
            if iou > 0.95:
                this_tracker_result.append(True)
            else:
                this_tracker_result.append(False)
            correct = all(this_tracker_result)
            if not correct:
                continue

            correct_id = tracker_id
            self.dead_trackers[correct_id].state = TrackState.Confirmed
            self.dead_trackers[correct_id].update(self.kf, detection, image, cur_frame, cur_time)
            self.tracked_objects.append(self.dead_trackers[correct_id])
            del self.dead_trackers[correct_id]
            return True
        return False

    def export_trackers(self):
        base_location = os.path.join(self.base_path,
                                     self.base_config.get_trackers_folder(),
                                     str(self.chain))
        if not os.path.isdir(base_location):
            os.mkdir(base_location)
        files = os.listdir(base_location)
        if not self.exported:
            if len(files) >= 1:
                for file in files:
                    os.remove(os.path.join(base_location, file))
            textFileLoc = os.path.join(base_location, self.base_config.get_tracker_data_file())
            with open(textFileLoc, 'w+') as file:
                file.write(str(self.tracking_id) + "\n")

            if self.tracked_objects:
                noneTrackers = 0
                for idx, tracked_obj in enumerate(self.tracked_objects):
                    if tracked_obj.correct_id is 0:
                        noneTrackers += 1
                        tracked_obj.correct_id = self.tracking_id + noneTrackers
                    tracked_obj.capture_image()
                    file_name = os.path.join(base_location, "tracker_" + str(tracked_obj.correct_id) + ".pkl")
                    with open(file_name, 'wb') as picko:
                        print("Starting Dumping tracker " + str(tracked_obj.correct_id))
                        pickle.dump(tracked_obj, picko, protocol=pickle.HIGHEST_PROTOCOL)
                        print("Dumping Complete of tracker " + str(tracked_obj.correct_id))
                self.exported = True

    def add_pickled_tracker(self, previous_obj):
        self.tracked_objects.append(previous_obj)
