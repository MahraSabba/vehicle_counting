# vim: expandtab:ts=4:sw=4
import os
import cv2
import math
import imageio
import numpy as np
from ..dense_tracking.States import TrackerStates, TrackState, VisibilityStates, LocalityStates


class Track:
    """
    A single target track with state space `(x, y, a, h)` and associated
    velocities, where `(x, y)` is the center of the bounding box, `a` is the
    aspect ratio and `h` is the height.

    Parameters
    ----------
    mean : ndarray
        Mean vector of the initial state distribution.
    covariance : ndarray
        Covariance matrix of the initial state distribution.
    track_id : int
        A unique track identifier.
    n_init : int
        Number of consecutive detections before the track is confirmed. The
        track state is set to `Deleted` if a miss occurs within the first
        `n_init` frames.
    max_age : int
        The maximum number of consecutive misses before the track state is
        set to `Deleted`.
    feature : Optional[ndarray]
        Feature vector of the detection this track originates from. If not None,
        this feature is added to the `features` cache.

    Attributes
    ----------
    mean : ndarray
        Mean vector of the initial state distribution.
    covariance : ndarray
        Covariance matrix of the initial state distribution.
    track_id : int
        A unique track identifier.
    hits : int
        Total number of measurement updates.
    age : int
        Total number of frames since first occurance.
    time_since_update : int
        Total number of frames since last measurement update.
    state : TrackState
        The current track state.
    features : List[ndarray]
        A cache of features. On each measurement update, the associated feature
        vector is added to this list.

    """
    image_saving_id = 1

    all_class_name = None

    def __init__(self, mean, covariance, track_id, n_init, max_age,
                 feature=None, detection_box=None, image=None, current_frame=None,
                 time=None, chain=None, score=None, category_name=None, base_path=None,
                 base_config=None):
        """
        :param score: The confidence score of the detection.
        :param category_name: The name of the class as denoted by the detection model.
        :param base_path: The base location from where the code is being run.
        :param mean: [x, y, a, h, 0, 0, 0, 0]
        :param covariance: covariance of the bbox
        :param track_id: The tracking id of this tracker
        :param n_init: The number of detections it takes to confirm this tracker as valid.
        :param max_age: The max frames this tracker can remain hidden after it has been confirmed.
        :param feature: The encoder extracted features of the detection
        :param detection_box: The x1, y1, x2, y2 points of the box
        :param image: The complete frame image.
        :param current_frame: The current frame number of the video.
        :param time: The current time of the video.
        :param chain: The local process iteration id for multiprocessing.
        """
        if base_config is None:
            from configuration import config
            base_config = config.Config()
        self.base_config = base_config
        self.mean = mean
        self.covariance = covariance
        self.state = TrackState.Tentative
        self.trackerstate = TrackerStates.PreInitialization
        self.visibilitystate = VisibilityStates.Visible
        self.localitystate = LocalityStates.InFOV
        self.tentative_limit = 3
        self.tentative_missed = 0
        self.track_id = track_id
        self._n_init = n_init  # minimum detections before changing state to confirmed.
        self._max_age = max_age  # maximum frame where the object could be missing but not be deleted.
        self.counted = False
        self.correct_id = 0
        self.hits = 1
        self.hit_streak = 0
        self.age = 1
        self.time_since_update = 0
        self.area_updated = 0
        self.cur_score = score
        self.scores = [score]
        self.features = []
        if feature is not None:
            self.features.append(feature)
        self.average_shape = None
        self.bbox = detection_box
        self.bboxes = [self.bbox]
        self.image = image
        self.image_height, self.image_width = self.image.shape[:2]
        self.image_aspect_ratio = math.ceil(self.image_width / self.image_height)
        self.current_frame = current_frame
        self.frames = [self.current_frame]
        self.chain = chain
        self.cur_time = time
        self.crop = None
        self.crops = []
        self.imageCaptured = False
        self.saving_base_location = base_path
        if image is not None:
            self.crop = self.get_crop_of_image()
            self.crops.append(self.crop)
        self.image_name = os.path.join(self.saving_base_location,
                                       base_config.get_tsim_null_image_name()) \
            if self.saving_base_location is not None \
            else os.path.join(os.getcwd(),
                              base_config.get_captured_images_folder(),
                              base_config.get_tsim_null_image_name())

        self.overall_class_names = []
        self.overall_class_num = []
        self.max_class_name = None
        self.max_class_num = None
        self.add_tracker_category_name(category_name, self.cur_score)

        self.history = []
        self.cur_mid_point = [0., 0.]  # Use a simple list
        self.midpoints = []  # Use a simple list of a list and convert asarray to np for calculation
        self.box_ratio = 0.  # height/width
        self.box_ratios = []  # Sames as midpoints. Convert to np when calculating
        self.cur_box_area = None
        self.box_areas = []
        self.max_crop_len = base_config.get_tsim_max_crops_allowed()
        self.detection_iter = 0

    def get_crop_of_image(self):
        """If the bbox value is in percentage, then crop after calculating the actual area. Otherwise,
           just crop out from the bbox itself."""
        if self.bbox[2] < 1.0:
            x1 = self.bbox[0] * self.image_width
            y1 = self.bbox[1] * self.image_height
            x2 = self.bbox[2] * self.image_width
            y2 = self.bbox[3] * self.image_height
            return self.image[int(y1):int(y2), int(x1):int(x2)]
        return self.image[int(self.bbox[1]):int(self.bbox[3]), int(self.bbox[0]):int(self.bbox[2])]

    def save_data_to_file(self, dataset_file_name, image_saving_directory):
        if self.correct_id == 0:
            cor_id = f'1-{Track.image_saving_id}'
            Track.image_saving_id += 1
            # self.capture_image(cor_id, image_saving_directory)
        else:
            cor_id = f'{self.correct_id}'
            # self.save_tracker_gif(cor_id, image_saving_directory)

        with open(dataset_file_name, 'a') as dataset_file:
            for frame_no, bbox in zip(self.frames, self.bboxes):
                x1, y1, x2, y2 = bbox
                if self.bbox[2] < 1.0:
                    x1, x2 = x1 * self.image_width, x2 * self.image_width
                    y1, y2 = y1 * self.image_height, y2 * self.image_height
                dataset_file.write(f'{cor_id}, {frame_no}, {x1}, {y1}, {x2}, {y2}\n')

    def capture_image(self, id_name, saving_directory):
        if self.crop is None:
            if len(self.crops) < 1:
                return
        self.image_name = os.path.join(saving_directory, f'{id_name}.jpg')
        crop_area = np.array([((width * height) for width, height in crop.shape[:2]) for crop in self.crops])
        try:
            largest_area = np.argmax(crop_area)
            self.crop = self.crops[largest_area]
        except:
            pass
        cv2.imwrite(self.image_name, self.crop)

    def save_tracker_gif(self, id_name, saving_directory):
        self.image_name = os.path.join(saving_directory, f'{id_name}.gif')
        print("Started {}".format(self.correct_id))
        with imageio.get_writer(self.image_name, mode='I') as writer:
            for obj_f_im in self.crops:
                im = cv2.cvtColor(obj_f_im, cv2.COLOR_BGR2RGB)
                writer.append_data(im)
            print("Done")

    def get_larger_crop(self):
        """Insert into the crops list.
           Check if the list has more than 1 item in it as the comparison needs at least 2 items in it.
           Get current crop shape and check if it isn't 0 in any dimension.
           If the current crop midpoint is closer to the bottom, then chose, depending on the box area."""
        self.crops.append(self.crop)
        if len(self.crops) <= 1:
            return
        cch, ccw = self.crop.shape[:2]
        if cch == 0 or ccw == 0:
            self.crop = self.crops[-2]
            return
        pch, pcw = self.crops[-2].shape[:2]
        if len(self.midpoints) >= 2:
            pmp = self.midpoints[-2]
            cmp = self.cur_mid_point
            if pmp[1] < cmp[1]:
                self.crop = self.crops[-2] if self.box_areas[-2] > self.cur_box_area else self.crop
            if self.box_ratio > 2 or self.box_ratio < 0.5:
                self.crop = self.crops[-2]
        if len(self.crops) > self.max_crop_len:
            self.crops.pop(0)

    def to_tlwh(self):
        """Get current position in bounding box format `(top left x, top left y,
        width, height)`.

        Returns
        -------
        ndarray
            The bounding box.

        """
        ret = self.mean[:4].copy()
        ret[2] *= ret[3]
        ret[:2] -= ret[2:] / 2
        return ret

    def to_tlbr(self):
        """Get current position in bounding box format `(min x, miny, max x,
        max y)`.

        Returns
        -------
        ndarray
            The bounding box.

        """
        ret = self.to_tlwh()
        ret[2:] = ret[:2] + ret[2:]
        return ret

    def predict(self, kf):
        """Propagate the state distribution to the current time step using a
        Kalman filter prediction step.

        Parameters
        ----------
        kf : kalman_filter.KalmanFilter
            The Kalman filter.

        """
        self.mean, self.covariance = kf.predict(self.mean, self.covariance)
        self.age += 1
        self.time_since_update += 1

    def update(self, kf, detection, image, current_frame, cur_time):
        """
        Perform Kalman filter measurement update step and update the feature
        cache.

        Parameters
        ----------
            :param kf: kalman_filter.KalmanFilter. The Kalman filter.
            :param detection: Detection. The associated detection.
            :param image: The current frame image of the video.
            :param current_frame: The current frame number of the video.
            :param cur_time: The current associated frame time.
        """
        self.cur_time = cur_time
        self.time_since_update = 0
        self.area_updated = 0
        self.current_frame = current_frame
        self.frames.append(self.current_frame)
        self.hits += 1
        self.hit_streak += 1
        self.time_since_update = 0
        self.cur_score = detection.confidence
        self.scores.append(self.cur_score)
        self.bbox = detection.to_tlbr()
        self.bboxes.append(self.bbox)
        self.mean, self.covariance = kf.update(
            self.mean, self.covariance, detection.to_xyah())
        self.features.append(detection.feature)
        self.image = image
        self.image_height, self.image_width = self.image.shape[:2]
        self.get_midpoint_of_box()
        if image is not None:
            self.crop = self.get_crop_of_image()
        self.get_larger_crop()

        if self.state == TrackState.Tentative and self.hits >= self._n_init:
            self.state = TrackState.Confirmed

    def get_midpoint_of_box(self):
        x1 = self.bbox[0] * self.image_width
        x2 = self.bbox[2] * self.image_width
        y1 = self.bbox[1] * self.image_height
        y2 = self.bbox[3] * self.image_height

        self.box_ratio = (y2 - y1) / (x2 - x1)  # aspect ratio
        self.box_ratios.append(self.box_ratio)
        self.cur_mid_point = [math.ceil((x1 + x2) / 2), math.ceil((y1 + y2) / 2)]
        self.midpoints.append(self.cur_mid_point)
        self.cur_box_area = (y2 - y1) * (x2 - x1)
        self.box_areas.append(self.cur_box_area)

    def add_tracker_category_name(self, category_name, category_score):
        threshold_score = 0.3
        if category_score > threshold_score:
            self.overall_class_names.append(category_name)
            for string_name, id_of_name in self.all_class_name.items():
                if category_name is string_name:
                    self.overall_class_num.append(id_of_name)
                    break
        if len(self.overall_class_num) > 0:
            self.max_class_num = max(set(self.overall_class_num), key=self.overall_class_num.count)
        if len(self.overall_class_names) > 0:
            self.max_class_name = max(set(self.overall_class_names), key=self.overall_class_names.count)

    def mark_missed(self):
        """Mark this track as missed (no association at the current time step).
        """
        if self.state == TrackState.Tentative:
            self.tentative_missed += 1
            if self.tentative_missed > self.tentative_limit:
                self.state = TrackState.Deleted
        elif self.time_since_update > 5 and self.in_ma():
            self.state = TrackState.Deleted
        elif self.time_since_update > self._max_age:
            self.state = TrackState.Deleted

        if self.visibilitystate == VisibilityStates.Visible:
            self.visibilitystate = VisibilityStates.GoingInvisible
        elif self.visibilitystate == VisibilityStates.GoingInvisible:
            self.visibilitystate = VisibilityStates.HiddenButThere

    def mark_visible(self):
        self.visibilitystate = VisibilityStates.Visible

    def set_locality_state(self, setter_id):
        setted = False
        if setter_id == 1:
            setted = True
            """ID 1 is AreaOfInterest"""
            self.mark_in_aoi()
        elif setter_id == 2:
            """ID 2 is MovementAreas"""
            setted = True
            self.mark_in_maoi()

        if not setted:
            self.mark_in_fov()

    def mark_in_fov(self):
        if self.in_aoi() or self.in_ma():
            self.localitystate = LocalityStates.AoITransition
        elif self.in_aoi_transition():
            self.localitystate = LocalityStates.InFOV

    def mark_in_aoi(self):
        if self.in_fov() or self.in_ma():
            self.localitystate = LocalityStates.AoITransition
        elif self.in_aoi_transition():
            self.localitystate = LocalityStates.InAoI

    def mark_in_maoi(self):
        if self.in_fov() or self.in_aoi():
            self.localitystate = LocalityStates.AoITransition
        elif self.in_aoi_transition():
            self.localitystate = LocalityStates.InMA

    """Track State Booleans"""
    def is_tentative(self):
        return self.state == TrackState.Tentative

    def is_confirmed(self):
        return self.state == TrackState.Confirmed

    def is_deleted(self):
        return self.state == TrackState.Deleted

    """Locality State Booleans"""
    def in_fov(self):
        return self.localitystate == LocalityStates.InFOV

    def in_ma(self):
        return self.localitystate == LocalityStates.InMA

    def in_aoi_transition(self):
        return self.localitystate == LocalityStates.AoITransition

    def in_aoi(self):
        return self.localitystate == LocalityStates.InAoI

    """Visibility State Booleans"""
    def is_visible(self):
        return self.visibilitystate == VisibilityStates.Visible

    def is_goinginvisible(self):
        return self.visibilitystate == VisibilityStates.GoingInvisible

    def is_hiddenbutthere(self):
        return self.visibilitystate == VisibilityStates.HiddenButThere

    """TrackerStates State Booleans"""
    def is_preinitialized(self):
        return self.trackerstate == TrackerStates.PreInitialization

    def is_initialized(self):
        return self.trackerstate == TrackerStates.Initialized

    def is_in_preremoval(self):
        return self.trackerstate == TrackerStates.PreRemoval

    def is_removed(self):
        return self.trackerstate == TrackerStates.Removed