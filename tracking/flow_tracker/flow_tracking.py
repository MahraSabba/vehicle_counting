import cv2
import numpy as np
from tracking.deep_sort.detection import Detection
from tracking.deep_sort.track import Track


class FlowTracker:

    def __init__(self):
        self.max_points = 30
        self.prev_ids = None
        self.prev_frame = None
        self.cur_ids = None
        self.cur_frame = None
        self.diff_x = None
        self.diff_y = None
        self.instance = 0
        self.win_size = (15, 15)
        self.max_level = 2
        self.prev_points = np.array([])
        self.cur_points = np.array([])
        self.point_tracker_dict = {}

    def set_image(self, cur_frame):
        if self.prev_frame is None:
            self.prev_frame = cv2.cvtColor(cur_frame, cv2.COLOR_BGR2GRAY)
        else:
            self.cur_frame = cv2.cvtColor(cur_frame, cv2.COLOR_BGR2GRAY)

    def set_points(self, cur_points, box_ids):
        if self.prev_points.size == 0:
            self.prev_points = cur_points
            self.prev_ids = box_ids
        else:
            self.cur_ids = box_ids
            self.cur_points = cur_points

    @staticmethod
    def __get_center_point_of_object(tracked_object):
        if isinstance(tracked_object, Detection):
            box = tracked_object.to_tlbr()
            box_id = None
        elif isinstance(tracked_object, Track):
            box = tracked_object.bbox
            box_id = tracked_object.track_id if tracked_object.track_id != 0 else None
        elif isinstance(tracked_object, np.ndarray):
            box = tracked_object[:4]
            box_id = tracked_object[4] if tracked_object[4] != 0 else None
        else:
            return None
        return [(box[0] + box[2]) / 2, (box[1] + box[3]) / 2], box_id

    def __get_points_inside_detections_boxes(self, midpoints, detected_object_bboxes):
        tracked_values = {}
        for bbox in detected_object_bboxes:
            if self.prev_points.size == 0:
                for prev_point_id, prev_point in enumerate(self.prev_points):
                    if bbox[0] < prev_point[0] < bbox[2] and \
                            bbox[1] < prev_point[1] < bbox[3]:
                        tracked_values[str(bbox[4])] = prev_point_id

    # @staticmethod
    # def get_point_displacement(input_points, estimated_point):
    #     diff_x = estimated_point[:, :1] - input_points[:, :1]
    #     diff_y = estimated_point[:, 1:] - input_points[:, 1:]
    #     return diff_x, diff_y

    # def find_closest_point(self, estimated_point, current_points):
    #     estimated_point = np.sqrt(np.square(estimated_point[:, 0]) + np.square(estimated_point[:, 1]))
    #     current_points = np.sqrt(np.square(current_points[:, 0]) + np.square(current_points[:, 1]))
    #     cross_subtraction = np.subtract.outer(estimated_point, current_points)
    #     min_val = np.argmin(cross_subtraction, axis=0)

    def update(self, cur_frame, tracked_object):
        center_points_with_ids = [self.__get_center_point_of_object(t_obj) for t_obj in tracked_object]
        midpoints, box_ids = zip(*center_points_with_ids)
        midpoints = np.array(midpoints, dtype=np.float32)
        box_ids = box_ids

        self.set_image(cur_frame)
        self.set_points(midpoints, box_ids)

        if self.prev_points is None:
            return
        if self.prev_points.size == 0:
            return

        # estimated_points, st, err = cv2.calcOpticalFlowPyrLK(self.prev_frame,
        #                                                self.cur_frame,
        #                                                midpoints,
        #                                                None,
        #                                                self.win_size,
        #                                                self.max_level)
        # self.diff_x, self.diff_y = self.get_point_displacement(self.prev_points, estimated_points)
        # self.find_closest_point(estimated_points, midpoints)
        self.prev_frame = self.cur_frame
        self.prev_points = midpoints  # estimated_points
        # np.concatenate(self.prev_points, estimated_points, axis=2)
        return midpoints #estimated_points
