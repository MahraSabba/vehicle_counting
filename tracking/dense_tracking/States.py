
class TrackerStates:
    PreInitialization = 1
    Initialized = 2
    PreRemoval = 5
    Removed = 6


class VisibilityStates:
    Visible = 1
    GoingInvisible = 2
    HiddenButThere = 3


class LocalityStates:
    InFOV = 1
    InMA = 2
    AoITransition = 3
    InAoI = 4


class TrackState:
    """
    Enumeration type for the single target track state. Newly created tracks are
    classified as `tentative` until enough evidence has been collected. Then,
    the track state is changed to `confirmed`. Tracks that are no longer alive
    are classified as `deleted` to mark them for removal from the set of active
    tracks.

    """
    Tentative = 1
    Confirmed = 2
    Deleted = 3
