import numpy as np
from tracking.dense_tracking import States


class MovementAreas(object):

    def __init__(self, aois):
        self.id = 2
        self.aois = aois
        self.aois_x1 = self.aois[:, 0]
        self.aois_y1 = self.aois[:, 1]
        self.aois_x2 = self.aois[:, 2]
        self.aois_y2 = self.aois[:, 3]
        self.aoi_areas = (self.aois_x2 - self.aois_x1 + 1) * (self.aois_y2 - self.aois_y1 + 1)
        self.current_objects = None
        self.current_detections = None

    def update(self, tracked_object_list, type='tracker'):

        if type == 'tracker':
            self.current_objects = self.get_dets_in_area(tracked_object_list, type=type)
            if not self.current_objects or self.current_objects is None:
                return
            for maoi in self.current_objects:
                for t_object in maoi:
                    if t_object.area_updated == 0:
                        t_object.set_locality_state(self.id)
                        t_object.area_updated = 1

        elif type == 'detection':
            self.current_detections = self.get_dets_in_area(tracked_object_list, type=type)
            if not self.current_detections or self.current_detections is None:
                return
            detections_in_maoi = []
            for maoi in self.current_detections:
                for detection in maoi:
                    detections_in_maoi.append(detection)
            return detections_in_maoi

    def get_dets_in_area(self, tracked_objects, type='tracker'):
        if type == 'tracker':
            return self.non_intersecting_suppression(tracked_objects, 1)
        elif type == 'detection':
            return self.non_intersecting_suppression(tracked_objects, 2)

    def non_intersecting_suppression(self, tracked_object_list, type, iou_threshold=0.6):
        if len(tracked_object_list) == 0:
            return [], []
        if type == 1:
            tracked_objects = np.asarray([trk.bbox for trk in tracked_object_list], dtype=np.float)
        elif type == 2:
            tracked_objects = np.asarray([det.to_tlbr() for det in tracked_object_list], dtype=np.float)
        x1 = tracked_objects[:, 0]
        y1 = tracked_objects[:, 1]
        x2 = tracked_objects[:, 2]
        y2 = tracked_objects[:, 3]
        object_area = (x2 - x1) * (y2 - y1)
        objects_in_aoi = []

        for aoi_number, aoi_area in enumerate(self.aoi_areas):
            xx1 = np.maximum(self.aois_x1[aoi_number], x1)
            yy1 = np.maximum(self.aois_y1[aoi_number], y1)
            xx2 = np.minimum(self.aois_x2[aoi_number], x2)
            yy2 = np.minimum(self.aois_y2[aoi_number], y2)
            w = np.maximum(0., xx2 - xx1 + 1.)
            h = np.maximum(0., yy2 - yy1 + 1.)
            overlapped_area = w * h

            overlapped_ratio = overlapped_area / object_area

            trackers = np.asarray(np.where(overlapped_ratio >= iou_threshold)).squeeze().tolist()

            if isinstance(trackers, list):
                cur_aoi_object_list = [tracked_object_list[i] for i in trackers]
                objects_in_aoi.append(cur_aoi_object_list)
            elif isinstance(trackers, int):
                cur_aoi_object_list = [tracked_object_list[trackers]]
                objects_in_aoi.append(cur_aoi_object_list)

        return objects_in_aoi
