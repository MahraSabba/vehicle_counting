#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

import os
import json
import warnings
import cv2
import numpy as np
from .deep_sort import preprocessing
from .deep_sort import nn_matching
from .deep_sort.detection import Detection
from .deep_sort.tracker import Tracker
from .tools import generate_detections as gdet


warnings.filterwarnings('ignore')


class SimilarityTracker:

    def __init__(self, base_dir, base_config=None, labels=None, aois=None, maois=None, clines=None):
        if base_config is None:
            from ..configuration import config
            base_config = config.Config()
        self.base_config = base_config

        if labels is None:
            labels = self.base_config.get_labels_file()

        self.max_cosine_distance = base_config.get_tsim_max_cosine_distance()
        self.nn_budget = base_config.get_tsim_nn_budget()
        self.nms_max_overlap = base_config.get_tsim_nms_overlap()
        self.base_dir = base_dir
        self.model_file_location = os.path.join(self.base_dir, base_config.get_similarity_model_location())
        self.encoder = gdet.create_box_encoder(self.model_file_location,
                                               batch_size=base_config.get_tsim_encoder_batch_size())
        self.distance_metric = nn_matching.NearestNeighborDistanceMetric(base_config.get_tsim_metric_type(),
                                                                         self.max_cosine_distance,
                                                                         self.nn_budget)
        self.tracker = Tracker      (self.base_dir,
                               sel                        f.distance_metric,
                               max_age=base_config.get_tsim_max_age(),
                               max_iou_distance=base_config.get_tsim_max_iou(),
                               n_init=base_config.get_tsim_n_init(),
                               base_config=base_config,
                               aois=aois,
                               maois=maois,
                               clines=clines)
        if isinstance(labels, str):
            self.tracker.initialize_track_dict(json.load(open(self.base_config.get_labels_file(), 'r')))
        else:
            self.tracker.initialize_track_dict(labels)

    def create_dataset_file(self, name):
        self.tracker.create_dataset_file(name)

    def initialize_tracker_object_dict(self, dictionary):
        self.tracker.initialize_track_dict(dictionary)

    def non_maxima_suppression_on_detection(self, detections):
        boxes = np.array([d.tlwh for d in detections])
        scores = np.array([d.confidence for d in detections])
        indices = preprocessing.non_max_suppression(boxes, self.nms_max_overlap, scores)
        detections = [detections[i] for i in indices]
        return detections

    def update(self, cur_frame_image, cur_frame_number, detections, category_name, category_score, this_time):
        features = self.encoder(cur_frame_image, detections)
        detections = [Detection(bbox, t_score, t_name, feature)
                      for bbox, feature, t_score, t_name in
                      zip(detections, features, category_score, category_name)]
        self.tracked_object_update(detections, cur_frame_image, cur_frame_number, this_time)
        return self.get_current_usable_trackers()

    def get_current_usable_trackers(self, time_since_update_threshold=1):
        tracked_objects = np.asarray([tracked_object.bbox for tracked_object in self.tracker.tracked_objects if
                                      tracked_object.time_since_update < time_since_update_threshold]).astype(int)
        tracked_ids = np.asarray([tracked_object.correct_id for tracked_object in self.tracker.tracked_objects if
                                  tracked_object.time_since_update < time_since_update_threshold])
        state = ["aoi" if trk.in_aoi() else "maoi" if trk.in_ma() else "fov" if trk.in_fov() else None
                 for trk in self.tracker.tracked_objects if trk.time_since_update < time_since_update_threshold]
        returnable_value = np.column_stack((tracked_objects, tracked_ids))
        return returnable_value, state

    def getOverallClass(self):
        class_name = [tracked_object.max_class_name for tracked_object in self.tracker.tracked_objects]
        return class_name

    def add_pickled_tracker(self, tracker):
        self.tracker.add_pickled_tracker(tracker)

    def export_trackers(self):
        self.tracker.export_trackers()

    def set_chain(self, chain):
        self.tracker.set_chain(chain)

    def set_tracking_id(self, tracking_id):
        self.tracker.tracking_id = tracking_id

    def tracked_object_update(self, detections, cur_frame_image, current_frame_number, current_time):
        """
        Prediction happens on all tracked objects. Thus the time since last update is
        incremented by 1 for all of them. If they are then matched and thus, then updated,
        their time since last update is reset to 0.

        detections  : The detection from the Object detection algorithm.
        cur_frame_image : Current RGB image tensor
        current_frame_number : The frame number for the video
        category_name : The string name of the category for the detection
        category_score : The accuracy score of the detected object
        current_time : The current time of the video.
        """
        self.tracker.predict()
        self.tracker.update(detections, cur_frame_image, current_frame_number, current_time)

    def insert_movement_paths_of_area(self, movement_paths):
        # Insert movement paths of area i.e. the boxes.
        pass

