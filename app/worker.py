from os import getcwd, path
import ray
import cv2
import time
import json
import numpy as np

from utils import utils
from configuration import config
from utils.Visualizer import Visualizer

from detection.VehicleDetector import VehicleDetector
from tracking.base_tracker import SimilarityTracker


@ray.remote(num_cpus=1, num_gpus=0.5)
class StreamWorker(object):
    def __init__(self, rois, stream_url, movement_areas, counting_lines, chain):
        self.name = "{}".format(time.time())
        self.stream_url = stream_url
        self.stream_name = path.basename(self.stream_url).split('.')[0]
        self.chain = chain
        self.stream = None
        self.playing = True
        self.cur_frame = 0
        self.frame_skip = 1
        self.base_config = config.Config()
        self.labels = json.load(open(self.base_config.get_labels_file(), 'r'))
        self.detector = VehicleDetector()
        self.aois = rois
        self.maois = movement_areas
        self.count_lines = counting_lines
        self.visualizer = Visualizer(labels={v: k for k, v in self.labels.items()})
        self.tracker = SimilarityTracker(base_dir=getcwd(),
                                         base_config=self.base_config,
                                         labels=self.labels,
                                         aois=rois,
                                         maois=movement_areas,
                                         clines=counting_lines)
        self.tracker.set_chain(chain)
        self.tracker.create_dataset_file(self.stream_name)
        self.window = cv2.namedWindow(self.name, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(self.name, (600, 400))
        self.keep_trying_to_play_seconds = 30
        self.not_returned_started = False
        self.start()

    def start(self):
        record_video = True
        self.counter = None
        print("{}".format(self.stream_url))

        try:
            self.stream = cv2.VideoCapture(self.stream_url)
            height, width = int(self.stream.get(cv2.CAP_PROP_FRAME_HEIGHT)), int(
                self.stream.get(cv2.CAP_PROP_FRAME_WIDTH))
            if record_video:
                video_fps = self.stream.get(cv2.CAP_PROP_FPS)
                fourcc = cv2.VideoWriter_fourcc(*'FMP4')
                video_location = path.join("/home/trafficgenius/TrafficGenius/vehicle_counting/data/videos",
                                           f"{self.stream_url.split('/')[-1].split('.')[0]}.mp4")
                video_writer = cv2.VideoWriter(video_location, fourcc, video_fps, (width, height))

            normalizer = np.array([width, height, width, height])
            while self.stream.isOpened():
                ret, frame = self.stream.read()
                self.cur_frame += 1
                if ret and self.cur_frame % self.frame_skip == 0:
                    processable_frame = frame.copy()
                    detections = self.detector.process_frame(processable_frame)
                    labels, boxes, scores = utils.threshold_detections(detections)
                    boxes = utils.normalize_boxes(boxes, normalizer)
                    tracked_detections, state = self.tracker.update(processable_frame,
                                                                    self.cur_frame,
                                                                    boxes,
                                                                    labels,
                                                                    scores,
                                                                    time.time())
                    # frame = self.visualizer.display_area(self.aois, frame, 'aoi')
                    # frame = self.visualizer.display_area(self.maois, frame, 'maoi')
                    # frame = self.visualizer.display_prediction(detections, frame)
                    frame = self.visualizer.display_tracked_objects(tracked_detections, state, frame)
                    if record_video:
                        video_writer.write(frame)
                    cv2.imshow(self.name, frame)
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        # with open(f"../data/CombinedJSONS/test_{self.chain}.json", 'w') as f:
                        #     json.dump(self.tracker.tracker.counter.count_data, f, indent=4)
                        self.playing = False
                        break
                if not ret:
                    self.playing = False
                    cv2.destroyAllWindows()
                    self.detector = None
                    break
        except Exception as exc:
            self.playing = False
            print(exc.with_traceback())

        self.stream.release()
        if record_video:
            video_writer.release()


    def wait_command(self):
        while self.playing:
            pass
        return True
